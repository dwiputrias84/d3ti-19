package aplikasi.penjadwalan;

import java.util.LinkedList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SesiUI {
    SimpleIntegerProperty sesi,keberangkatan;
    SimpleStringProperty kapal,nahkoda,pelabuhan,rute;
    
    SesiUI(int sesi,int keberangkatan,SimpleStringProperty kapal,SimpleStringProperty nahkoda,String pelabuhan){
        this.sesi=new SimpleIntegerProperty(sesi);
        this.keberangkatan=new SimpleIntegerProperty(keberangkatan);
        this.kapal= kapal;
        this.nahkoda= nahkoda;
        this.pelabuhan= new SimpleStringProperty(pelabuhan);
        DBHandler db = DBHandler.getDatabaseHandler();
        String temp = "";
        LinkedList<Pelabuhan> r = Pelabuhan.getRute(db.getPelabuhan("Ajibata"), db.getPelabuhan(pelabuhan), db.pelabuhans.size());
        for (Pelabuhan pelabuhan2 : r) {
            if (temp.length()!=0) {
                temp+="->";
            }
            temp+=pelabuhan2.getNama();
        }
        rute = new SimpleStringProperty(temp);
    }
}
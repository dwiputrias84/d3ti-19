/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class ExportExcelController implements Initializable {

    @FXML
    private AnchorPane outputPane;
    @FXML
    private TableView<?> tblDataJadwal;
    @FXML
    private TableColumn<?, ?> tblKolomHari;
    @FXML
    private TableColumn<?, ?> tblKolomSesi;
    @FXML
    private TableColumn<?, ?> tblKolomKeberangkatan;
    @FXML
    private TableColumn<?, ?> tblKolomKapal;
    @FXML
    private TableColumn<?, ?> tblKolomNahkoda;
    @FXML
    private TableColumn<?, ?> tblKolomKategori;
    @FXML
    private TableColumn<?, ?> tblKolomWilayah;
    @FXML
    private ComboBox<?> hariCombo;
    @FXML
    private Button btnDashboard;
    @FXML
    private Text totalData;
    @FXML
    private Button btnExport;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void onClickHariCombo(ActionEvent event) {
    }

    @FXML
    private void toDashboard(ActionEvent event) {
    }

    @FXML
    private void exportToExcel(ActionEvent event) {
    }
    
}

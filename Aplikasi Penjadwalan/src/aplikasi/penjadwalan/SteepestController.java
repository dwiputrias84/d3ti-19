/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

import jxl.CellView;
import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class SteepestController implements Initializable {

    @FXML
    private AnchorPane dijkstraPane;
    @FXML
    private Spinner<Integer> maxKebe;
    @FXML
    private ComboBox<?> hariCombo;
    @FXML
    private TableView<SesiUI> tblDataJadwal;
    @FXML
    private TableColumn<SesiUI, Integer> tblKolomSesi;
    @FXML
    private TableColumn<SesiUI, Integer> tblKolomKeberangkatan;
    @FXML
    private TableColumn<SesiUI, String> tblKolomMatkul;
    @FXML
    private TableColumn<SesiUI, String> tblKolomDosen;
    @FXML
    private TableColumn<SesiUI, String> tblKolomPelabuhan;
    @FXML
    private TableColumn<SesiUI,String> tblKolomRute;
    @FXML
    private Button btnDashboard;
    @FXML
    private Button btnGenerateDijkstra;
    @FXML
    private Button btnToOutput;
    @FXML
    private Text totalData;
    DBHandler db;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db = DBHandler.getDatabaseHandler();
        maxKebe.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE));
        tblDataJadwal.setItems(db.jadwal.property);
        tblKolomSesi.setCellValueFactory(value -> value.getValue().sesi.asObject());
        tblKolomKeberangkatan.setCellValueFactory(value -> value.getValue().keberangkatan.asObject());
        tblKolomMatkul.setCellValueFactory(value -> value.getValue().kapal);
        tblKolomDosen.setCellValueFactory(value -> value.getValue().nahkoda);
        tblKolomPelabuhan.setCellValueFactory(value -> value.getValue().pelabuhan);
        tblKolomRute.setCellValueFactory(value->value.getValue().rute);
    }

    @FXML
    private void onClickHariCombo(ActionEvent event) {
    }

    @FXML
    private void toDashboard(ActionEvent event) {
        dijkstraPane.getScene().setRoot(AplikasiPenjadwalan.dashboard);
    }

    @FXML
    private void generateDijkstra(ActionEvent event) {
        db.jadwal.generateJadwal(db.kapals.values(), maxKebe.getValue());
    }

    @FXML
    private void toExcel(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("SphreadShit (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(AplikasiPenjadwalan.stage);

        if (file != null) {
            try {
                WritableWorkbook workbook = Workbook.createWorkbook(file);
                workbook.createSheet("Jadwal", 0);
                WritableSheet worksheet = workbook.getSheet(0);
                // Create Label
                // Lets create a times font
                WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
                // Define the cell format
                WritableCellFormat times = new WritableCellFormat(times10pt);
                // Lets automatically wrap the cells
                times.setWrap(true);

                // create create a bold font with unterlines
                WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false,
                        UnderlineStyle.SINGLE);
                WritableCellFormat timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
                // Lets automatically wrap the cells
                timesBoldUnderline.setWrap(true);

                CellView cv = new CellView();
                cv.setFormat(times);
                cv.setFormat(timesBoldUnderline);
                cv.setAutosize(true);

                // Write a few headers
                addCaption(worksheet, 0, 0, "Sesi", timesBoldUnderline);
                addCaption(worksheet, 1, 0, "Keberangkatan", timesBoldUnderline);
                addCaption(worksheet, 2, 0, "Kapal", timesBoldUnderline);
                addCaption(worksheet, 3, 0, "Nahkoda", timesBoldUnderline);
                addCaption(worksheet, 4, 0, "Pelabuhan", timesBoldUnderline);
                addCaption(worksheet, 5, 0, "Rute", timesBoldUnderline);

                for(int i = 1;i<=db.jadwal.property.size();i++){
                    SesiUI sesi = db.jadwal.property.get(i-1);
                    // Write a content
                    try {
                        addCaption(worksheet, 0, i, Integer.toString(sesi.sesi.get()), times);
                        addCaption(worksheet, 1, i, Integer.toString(sesi.keberangkatan.get()),times);
                        addCaption(worksheet, 2, i, sesi.kapal.get(),times);
                        addCaption(worksheet, 3, i, sesi.nahkoda.get(),times);
                        addCaption(worksheet, 4, i, sesi.pelabuhan.get(),times);
                        addCaption(worksheet, 5, i, sesi.rute.get(),times);
                    } catch (WriteException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        db.error("gagal menulis cell");
                    }
                }

                workbook.write();
                workbook.close();
            } catch (WriteException e) {
                db.error("Gagal menulis File");
            } catch (IOException e) {
                db.error("Gagal menulis File");
            }
        }

    }

    private void addCaption(WritableSheet sheet, int column, int row, String s,
            CellFormat timesBoldUnderline)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }
    
}

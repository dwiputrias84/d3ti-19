/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class CspController implements Initializable {

    @FXML
    private AnchorPane cspPane;
    @FXML
    private Text text;
    @FXML
    private Spinner<Integer> sesiCombo;
    @FXML
    private Spinner<Integer> keberangkatanCombo;
    @FXML
    private ComboBox<Kapal> kapalCombo;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnHapusSesiKapalNahkoda;
    @FXML
    private TableView<SesiUI> tblDataJadwal;
    @FXML
    private TableColumn<SesiUI, Integer> tblKolomSesi;
    @FXML
    private TableColumn<SesiUI, Integer> tblKolomKeberangkatan;
    @FXML
    private TableColumn<SesiUI, String> tblKolomKapal;
    @FXML
    private Button btnDashboard;
    @FXML
    private Button btnGenerateJadwal;
    @FXML
    private Text totalData;
    @FXML
    private Button btnToGenerateDijkstra;
    private DBHandler db;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db=DBHandler.getDatabaseHandler();
        sesiCombo.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,Integer.MAX_VALUE));
        keberangkatanCombo.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,Integer.MAX_VALUE));
        tblDataJadwal.setItems(db.jadwal.property);
        tblKolomSesi.setCellValueFactory(value->value.getValue().sesi.asObject());
        tblKolomKeberangkatan.setCellValueFactory(value->value.getValue().keberangkatan.asObject());
        tblKolomKapal.setCellValueFactory(value->value.getValue().kapal);
        kapalCombo.setItems(db.kapalProperty);
    }    

    @FXML
    private void onClicksesiCombo(ActionEvent event) {
    }

    @FXML
    private void onClickKapalCombo(ActionEvent event) {
    }

    @FXML
    private void onClickNahkodaCombo(ActionEvent event) {
    }

    @FXML
    private void tambahJadwalAction(ActionEvent event) {
        db.jadwal.addRelasi(sesiCombo.getValue(), keberangkatanCombo.getValue(), kapalCombo.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void hapusSesiKapalNahkodaAction(ActionEvent event) {
        db.jadwal.clear();
    }

    @FXML
    private void toDashboard(ActionEvent event) {
        cspPane.getScene().setRoot(AplikasiPenjadwalan.dashboard);
    }

    @FXML
    private void generateJadwal(ActionEvent event) {
    }

    @FXML
    private void toDatabase(ActionEvent event) {
        db.saveJadwal();
    }
    
}
